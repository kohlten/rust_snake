extern crate sfml;
extern crate rand;

mod snake;
mod food;
mod game;

use sfml::system::{ Vector2u };

fn main() {
    let mut game = game::Game::new(Vector2u::new(400, 400), Default::default(), "Snake Game in Rust");
    game.run();
}
