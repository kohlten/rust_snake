use std::collections::VecDeque;

use sfml::graphics::*;
use sfml::system::*;

// @TODO Change this to a config variable or a game const
const SNAKE_SPEED: i32 = 10;

#[derive(PartialEq)]
pub enum SnakeDirection {
    Up = 1,
    Down = 2,
    Left = 3,
    Right = 4,
    None = 0,
}

pub struct Snake {
    pos: Vector2i,
    dir: SnakeDirection,
    died: bool,
    previous_positions: VecDeque<Vector2i>,
    max_length: u64,
    length: u64,
}

impl Snake {
    pub fn new() -> Snake {
        Snake {
            pos: Vector2i::new(0, 0),
            dir: SnakeDirection::None,
            died: false,
            previous_positions: VecDeque::new(),
            max_length: 0,
            length: 0,
        }
    }

    pub fn update(&mut self, window_size: Vector2i) {
        if !self.died && self.dir != SnakeDirection::None {
            if self.length >= self.max_length && self.max_length > 0 {
                self.previous_positions.pop_back();
                self.length -= 1;
            }
            if self.max_length > 0 {
                self.previous_positions.push_front(self.pos);
                self.length += 1;
            }
            match self.dir {
                SnakeDirection::Up => self.pos.y -= SNAKE_SPEED,
                SnakeDirection::Down => self.pos.y += SNAKE_SPEED,
                SnakeDirection::Left => self.pos.x -= SNAKE_SPEED,
                SnakeDirection::Right => self.pos.x += SNAKE_SPEED,
                SnakeDirection::None => {}
            }
            if self.pos.x < 0 || self.pos.y < 0 || self.pos.x >= window_size.x || self.pos.y >= window_size.y {
                self.died = true;
            }
            for pos in self.previous_positions.iter() {
                if pos.x == self.pos.x && pos.y == self.pos.y {
                    self.died = true;
                }
            }
        }
    }

    pub fn draw(&mut self, window: &mut RenderWindow) {
        if !self.died {
            let mut shape = RectangleShape::new();
            shape.set_size(Vector2f::new(SNAKE_SPEED as f32, SNAKE_SPEED as f32));
            shape.set_fill_color(&Color::RED);
            shape.set_position(Vector2f::new(self.pos.x as f32, self.pos.y as f32));
            window.draw(&shape);
            for pos in self.previous_positions.iter() {
                shape.set_position(Vector2f::new(pos.x as f32, pos.y as f32));
                window.draw(&shape);
            }
        }
    }

    pub fn reset(&mut self) {
        self.pos = Vector2i::new(0, 0);
        self.dir = SnakeDirection::None;
        self.died = false;
        self.previous_positions = VecDeque::new();
        self.max_length = 0;
        self.length = 0;
    }

    pub fn get_pos(&mut self) -> Vector2i {
        self.pos
    }

    pub fn set_dir(&mut self, dir: SnakeDirection) { self.dir = dir; }

    pub fn add_to_tail(&mut self) { self.max_length += 1; }

    pub fn get_tail_positions(&mut self) -> Vec<&Vector2i> { self.previous_positions.iter().collect() }

    pub fn is_dead(&self) -> bool { self.died }
}