use sfml::graphics::*;
use sfml::system::*;
use rand::random;

// @TODO Change this to a config variable or a game const
const FOOD_SIZE: i32 = 10;

pub struct Food {
    pos: Vector2i,
}

impl Food {
    pub fn new() -> Food {
        Food { pos: Vector2i::new(0, 0) }
    }

    pub fn draw(&mut self, window: &mut RenderWindow) {
        let mut shape = RectangleShape::new();
        shape.set_size(Vector2f::new(FOOD_SIZE as f32, FOOD_SIZE as f32));
        shape.set_fill_color(&Color::GREEN);
        shape.set_position(Vector2f::new(self.pos.x as f32, self.pos.y as f32));
        window.draw(&shape);
    }

    fn generate_new_pos(&mut self, window_size: Vector2i) -> Vector2i {
        let mut new_pos = Vector2i::new(0, 0);
        new_pos.x = (random::<u32>() % (window_size.x as u32 / FOOD_SIZE as u32)) as i32;
        new_pos.y = (random::<u32>() % (window_size.y as u32 / FOOD_SIZE as u32)) as i32;
        new_pos.x = new_pos.x * FOOD_SIZE;
        new_pos.y = new_pos.y * FOOD_SIZE;
        new_pos
    }

    pub fn new_pos(&mut self, window_size: Vector2i, snake_positions: Vec<&Vector2i>) {
        let mut new_pos = Vector2i::new(0, 0);
        let mut found = true;
        while found {
            new_pos = self.generate_new_pos(window_size);
            found = false;
            for pos in snake_positions.iter() {
                if pos.x == new_pos.x && pos.y == new_pos.y { found = true; }
            }
        }
        self.pos = new_pos;
    }

    pub fn get_pos(&mut self) -> Vector2i { self.pos }
}