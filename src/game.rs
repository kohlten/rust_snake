use sfml::graphics::*;
use sfml::system::*;
use sfml::window::*;

use snake::{Snake, SnakeDirection};
use food::Food;

pub struct Game {
    window: RenderWindow,
    running: bool,
    snake: Snake,
    food: Food,
    clock: Clock
}

impl Game {
    pub fn new(size: Vector2u, settings: ContextSettings, title: &str) -> Game {
        let video_mode = VideoMode::new(size.x, size.y, 32);
        let window = RenderWindow::new(video_mode, title, Style::CLOSE, &settings);
        let snake = Snake::new();
        let mut food = Food::new();
        let mut game;
        food.new_pos(Vector2i::new(size.x as i32, size.y as i32), Vec::new());
        game = Game { window, running: true, snake, food, clock: Clock::start()};
        game.window.set_vertical_sync_enabled(true);
        game
    }

    fn events(&mut self) {
        while let Some(event) = self.window.poll_event() {
            match event {
                Event::Closed => self.running = false,
                Event::KeyPressed { code: Key::W, ..} => self.snake.set_dir(SnakeDirection::Up),
                Event::KeyPressed { code: Key::S, ..} => self.snake.set_dir(SnakeDirection::Down),
                Event::KeyPressed { code: Key::A, ..} => self.snake.set_dir(SnakeDirection::Left),
                Event::KeyPressed { code: Key::D, ..} => self.snake.set_dir(SnakeDirection::Right),
                _ => {}
            }
        }
    }

    fn update(&mut self) {
        let window_size = self.window.size();
        let snake_pos;
        let food_pos;
        self.snake.update(Vector2i::new(window_size.x as i32, window_size.y as i32));
        snake_pos = self.snake.get_pos();
        food_pos = self.food.get_pos();
        if snake_pos.x == food_pos.x && snake_pos.y == food_pos.y {
            self.food.new_pos(Vector2i::new(window_size.x as i32, window_size.y as i32), self.snake.get_tail_positions());
            self.snake.add_to_tail();
        }
        if self.snake.is_dead() {
            self.snake.reset();
        }
    }

    fn draw(&mut self) {
        self.window.clear(&Color::BLACK);
        self.food.draw(&mut self.window);
        self.snake.draw(&mut self.window);
        self.window.display();
    }

    pub fn run(&mut self) {
        let mut last_time = self.clock.elapsed_time().as_milliseconds();
        while self.running {
            self.events();
            if self.clock.elapsed_time().as_milliseconds() - last_time >= 100 {
                self.update();
                self.clock.restart();
                last_time = self.clock.elapsed_time().as_milliseconds();
            }
            self.draw();
        }
    }
}